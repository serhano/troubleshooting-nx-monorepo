interface Locale {
  countryCode: string;
  scriptCode?: string;
  languageTag: string;
  languageCode: string;
  isRTL: boolean;
}


const getLocales = (): Locale[] => [
  { countryCode: 'US', languageTag: 'en-US', languageCode: 'en', isRTL: false },
  { countryCode: 'TR', languageTag: 'tr-TR', languageCode: 'tr', isRTL: false },
];

const findBestAvailableLanguage = (): Partial<Locale> => ({
  languageTag: 'en-US',
  isRTL: false,
});

const getNumberFormatSettings = (): object => ({
  decimalSeparator: '.',
  groupingSeparator: ',',
});

const getCalendar = (): string => 'gregorian'; // or "japanese", "buddhist"
const getCountry = (): string => 'US'; // the country code you want
const getCurrencies = (): [string] => ['USD']; // can be empty array
const getTemperatureUnit = (): string => 'celsius'; // or "fahrenheit"
const getTimeZone = (): string => 'Europe/Paris'; // the timezone you want
const uses24HourClock = (): boolean => true;
const usesMetricSystem = (): boolean => true;

const addEventListener = jest.fn();
const removeEventListener = jest.fn();
const addListener = addEventListener;
const removeListener = removeEventListener;


// let constants: LocalizationConstants = RNLocalize.initialConstants;
const initialConstants = {
  calendar: getCalendar(),
  country: getCountry(),
  currencies: getCurrencies(),
  locales: getLocales(),
  numberFormatSettings: getNumberFormatSettings(),
  temperatureUnit: getTemperatureUnit(),
  timeZone: getTimeZone(),
  uses24HourClock: uses24HourClock(),
  usesMetricSystem: usesMetricSystem(),
};


export {
  findBestAvailableLanguage,
  getLocales,
  getNumberFormatSettings,
  getCalendar,
  getCountry,
  getCurrencies,
  getTemperatureUnit,
  getTimeZone,
  uses24HourClock,
  usesMetricSystem,
  addEventListener,
  removeEventListener,
  addListener,
  removeListener,
  initialConstants
};
