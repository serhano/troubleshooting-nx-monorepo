import * as React from 'react';
import { render } from '@testing-library/react-native';

import App from './App';

test('renders correctly', () => {
    const { toJSON } = render(<App />);
    const tree = toJSON();
    console.log('tree');
    console.log(tree);
    expect(tree).toMatchSnapshot();
});
