import React from 'react';

import { ApplicationWrapper } from '@multiplatform-app/shared/feature/root-navigation';

const App = () => {
    return <ApplicationWrapper />;
};

export default App;
