import React from 'react'

import { ApplicationWrapper } from '@multiplatform-app/shared/feature/root-navigation'

export function App() {
    return <ApplicationWrapper />
}

export default App
