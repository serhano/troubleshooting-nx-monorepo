import { render } from '@testing-library/react';

import App from './app';

describe('App', () => {
    it('should render successfully', () => {
        const { baseElement } = render(<App />);
        expect(true).toBeTruthy();
    });

    it('should have a greeting as the title', () => {
        const { getByText } = render(<App />);

        expect(getByText("Let's learn in playtime!")).toBeTruthy();
    });
});
