// import { StrictMode } from 'react';
// import * as ReactDOM from 'react-dom';

// import App from './app/app';

// ReactDOM.render(
//   <StrictMode>
//     <App />
//   </StrictMode>,
//   document.getElementById('root')
// );

import { AppRegistry } from 'react-native'
import { App } from './app/app'

AppRegistry.registerComponent('main', () => App)
AppRegistry.runApplication('main', {
    rootTag: document.getElementById('root'),
})
