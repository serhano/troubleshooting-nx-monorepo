module.exports = {
    displayName: 'web-app',
    preset: '../../jest.preset.js',
    transform: {
        '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '@nrwl/react/plugins/jest',
        '^.+\\.[tj]sx?$': 'babel-jest',
    },
    transformIgnorePatterns: [
        'node_modules/(?!((jest-)?react-native(-.*)?|@react-native(-community)?)/)',
    ],
    // moduleNameMapper: {"react-native": "<rootDir>/node_modules/react-native-web"},
    setupFilesAfterEnv: ['../../__tests__/setup/test-setup-web.ts'],
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
    coverageDirectory: '../../coverage/apps/web-app',
};
