module.exports = {
    displayName: 'shared-feature-root-navigation',
    preset: '../../../../jest.preset.js',
    transform: {
        '^.+\\.[tj]sx?$': 'babel-jest',
    },
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
    coverageDirectory:
        '../../../../coverage/libs/shared/feature/root-navigation',
}
