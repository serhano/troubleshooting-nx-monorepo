import React, { useEffect } from 'react';
import * as RNLocalize from 'react-native-localize';
import {
    setI18nConfig,
    initI18n,
    translate,
} from '@multiplatform-app/shared/util/i18n';

import { ApplicationFrame, Header } from '@multiplatform-app/shared/ui/layout';
import { RootNavigator } from '../root-navigator/root-navigator';

initI18n();

/* eslint-disable-next-line */
export interface ApplicationWrapperProps {}

export function ApplicationWrapper(props: ApplicationWrapperProps) {
    useEffect(() => {
        initI18n();
    }, []);

    useEffect(() => {
        const handleLocalizationChange = (): void => {
            setI18nConfig();
        };
        RNLocalize.addEventListener('change', handleLocalizationChange);
        return function cleanup(): void {
            RNLocalize.removeEventListener('change', handleLocalizationChange);
        };
    });

    return (
        <ApplicationFrame>
            <Header text={translate('pages.welcome.welcome-message')} />
            {/* <RootNavigator /> */}
        </ApplicationFrame>
    );
}

export default ApplicationWrapper;
