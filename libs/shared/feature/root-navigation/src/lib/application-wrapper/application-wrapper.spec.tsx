import React from 'react'
import { render } from '@testing-library/react-native'

import ApplicationWrapper from './application-wrapper'

describe('ApplicationWrapper', () => {
    it('should render successfully', () => {
        const { container } = render(<ApplicationWrapper />)
        expect(container).toBeTruthy()
    })
})
