import { Platform, StyleSheet } from 'react-native'

const scrollViewPaddingTop = Platform.select({
    ios: 0,
    android: 0,
})

const appFrameStyles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'transparent',
        height: '100%',
        paddingTop: scrollViewPaddingTop,
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    statusBar: { flex: 0 },
    imageBackground: {
        flex: 1,
    },
})

export default appFrameStyles

const statusBarStyle = 'dark-content' // or 'light-content'
const dummyRuleColor = 'transparent'

// We prepare and feed this theme to the Authenticator component
// with the sole purpose of making the SafeAreaView bottom section
// transparent. We achieve that by giving a flex: 0 to container.
// the rest of the rules are here to meet type validation
export { statusBarStyle }
