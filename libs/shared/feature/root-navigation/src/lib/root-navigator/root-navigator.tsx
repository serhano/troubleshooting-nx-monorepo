import React from 'react'
import { View, Text } from 'react-native'
import { NativeRouter, Route, Routes, Link } from 'react-router-native'

/* eslint-disable-next-line */
export interface RootNavigatorProps {
    authState?: any
}

function Home() {
    return <Text>Home page</Text>
}

function About() {
    return <Text>About page</Text>
}

export function RootNavigator(props: RootNavigatorProps) {
    const { authState } = props

    return (
        <NativeRouter>
            <View>
                <Text>!!{authState}</Text>

                <Link to="/" underlayColor="#f0f4f7">
                    <Text>Home</Text>
                </Link>
                <Link to="/about" underlayColor="#f0f4f7">
                    <Text>About</Text>
                </Link>
                <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/about" element={<About />} />
                </Routes>
            </View>
        </NativeRouter>
    )
}

export default RootNavigator
