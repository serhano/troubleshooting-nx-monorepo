import * as React from 'react';
import { View, Text } from 'react-native';

/* eslint-disable-next-line */
export interface HeaderProps {
    text: string;
}

export function Header(props: HeaderProps) {
    return (
        <View>
            <Text testID="heading">{props.text}</Text>
        </View>
    );
}

export default Header;
