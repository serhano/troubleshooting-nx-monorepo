import React from 'react'
import { render } from '@testing-library/react-native'

import ApplicationFrame from './application-frame'

describe('ApplicationFrame', () => {
    it('should render successfully', () => {
        const { container } = render(<ApplicationFrame />)
        expect(container).toBeTruthy()
    })
})
