import { Platform, StyleSheet, StatusBar } from 'react-native';
import { Colors, UNIT_MARGIN } from '@multiplatform-app/shared/ui/theme';

const scrollViewPaddingTop = Platform.select({
    ios: 0,
    android: StatusBar.currentHeight
        ? StatusBar.currentHeight + UNIT_MARGIN
        : UNIT_MARGIN,
});

const appFrameStyles = StyleSheet.create({
    scrollView: {
        backgroundColor: 'transparent',
        height: '100%',
        paddingTop: scrollViewPaddingTop,
    },
    contentContainerStyle: {
        flexGrow: 1,
    },
    statusBar: {
        flex: 0,
        backgroundColor: Colors.STATUS_BAR_COLOR,
    },
    imageBackground: {
        flex: 1,
        backgroundColor: Colors.BASE_WHITE,
    },
});

export default appFrameStyles;

const statusBarStyle = 'dark-content'; // or 'light-content'

export { statusBarStyle };

// We prepare and feed this theme to the Authenticator component
// with the sole purpose of making the SafeAreaView bottom section
// transparent. We achieve that by giving a flex: 0 to container.
// the rest of the rules are here to meet type validation

// const dummyRuleColor = 'transparent'

// const AmplifyTheme = StyleSheet.create({
//     container: {
//         flex: 0,
//     },
//     section: {
//         backgroundColor: dummyRuleColor,
//     },
//     sectionHeader: {
//         backgroundColor: dummyRuleColor,
//     },
//     sectionHeaderText: {
//         backgroundColor: dummyRuleColor,
//     },
//     sectionFooter: {
//         backgroundColor: dummyRuleColor,
//     },
//     sectionFooterLink: {
//         backgroundColor: dummyRuleColor,
//     },
//     navBar: {
//         backgroundColor: dummyRuleColor,
//     },
//     navButton: {
//         backgroundColor: dummyRuleColor,
//     },
//     cell: {
//         backgroundColor: dummyRuleColor,
//     },
//     errorRow: {
//         backgroundColor: dummyRuleColor,
//     },
//     errorRowText: {
//         backgroundColor: dummyRuleColor,
//     },
//     photo: {
//         backgroundColor: dummyRuleColor,
//     },
//     album: {
//         backgroundColor: dummyRuleColor,
//     },
//     button: {
//         backgroundColor: dummyRuleColor,
//     },
//     buttonDisabled: {
//         backgroundColor: dummyRuleColor,
//     },
//     buttonText: {
//         backgroundColor: dummyRuleColor,
//     },
//     formField: {
//         backgroundColor: dummyRuleColor,
//     },
//     input: {
//         backgroundColor: dummyRuleColor,
//     },
//     inputLabel: {
//         backgroundColor: dummyRuleColor,
//     },
//     phoneContainer: {
//         backgroundColor: dummyRuleColor,
//     },
//     phoneInput: {
//         backgroundColor: dummyRuleColor,
//     },
//     picker: {
//         backgroundColor: dummyRuleColor,
//     },
//     pickerItem: {
//         backgroundColor: dummyRuleColor,
//     },
// })

// export { AmplifyTheme, statusBarStyle }
