import React, { memo, ReactNode } from 'react';
import {
    ScrollView,
    ImageBackground,
    StatusBar,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform,
} from 'react-native';

import appFrameStyles, { statusBarStyle } from './application-frame.styles';
import { bgImage } from '@multiplatform-app/shared/ui/theme';

/* eslint-disable-next-line */
export interface ApplicationFrameProps {
    children: ReactNode;
}

export const ApplicationFrame = memo(({ children }: ApplicationFrameProps) => {
    return (
        <ImageBackground
            source={bgImage}
            style={appFrameStyles.imageBackground}
        >
            <StatusBar
                translucent
                backgroundColor={appFrameStyles.statusBar.backgroundColor}
                barStyle={statusBarStyle}
            />
            {/* <StatusBar translucent backgroundColor="transparent" /> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                enabled
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <ScrollView
                        contentInsetAdjustmentBehavior="automatic"
                        contentContainerStyle={
                            appFrameStyles.contentContainerStyle
                        }
                        style={appFrameStyles.scrollView}
                    >
                        {children}
                    </ScrollView>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </ImageBackground>
    );
});

export default ApplicationFrame;
