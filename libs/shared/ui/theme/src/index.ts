import * as Colors from './lib/palette'
export { Colors }
export * from './lib/config'
export const bgImage = require('./assets/images/app-bg.png')
export const logo = require('./assets/images/logo.png')
