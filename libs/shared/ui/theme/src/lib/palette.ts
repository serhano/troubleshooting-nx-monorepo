// Color pallete for the theme
export const BASE_WHITE = '#ffffff';
export const BASE_BLACK = '#000000';

// export const BASE_FONT_COLOR = '#141414';

// export const LOGO_LIGHT_GREEN = '#8ac53e';
// export const LOGO_DARK_GREEN = '#006838';
// export const LOGO_GREENISH_YELLOW = '#d5de22';

// export const LIGHT_GREENISH_GRAY = '#c8dcd4';

// export const BACKGROUND_VERY_LIGHT_GREEN = '#bfea8c';
// export const BACKGROUND_YELLOWISH_WHITE = '#fafbe9';

// export const STATUS_BAR_COLOR = BASE_WHITE
export const STATUS_BAR_COLOR = 'transparent';

// // Forms
// export const INPUT_FIELD_BACKGROUND_COLOR = '#fafafa';
// export const INPUT_FIELD_BORDER_COLOR = '#dddddd';
// export const INPUT_FIELD_PLACEHOLDER_COLOR = '#a8a8a8';

// export const ERROR_MESSAGES_FONT_COLOR = '#e35256';
