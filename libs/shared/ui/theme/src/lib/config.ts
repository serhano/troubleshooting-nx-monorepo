import { Dimensions } from 'react-native'

// // UI variables configuration
export const win = Dimensions.get('window')
export const isPortrait = (): boolean => {
    return win.height >= win.width
}
export const isLandscape = (): boolean => {
    return !isPortrait()
}
const unitMeasure = isPortrait() ? win.width / 100 : win.height / 100
export const vw = unitMeasure
export const UNIT_MARGIN = 5 * vw
export const FORM_WIDTH = 92 * vw
export const LOGO_WIDTH = 70 * vw
export const LOGO_PXL_RESOLUTION = { wide: 1520, tall: 437 }

// // Font configuration
export const BASE_FONT_SIZE = 6 * vw
export const BASE_ICON_SIZE = 5 * vw
export const TITLE_FONT_SIZE = BASE_FONT_SIZE * 1.4
export const ERROR_MESSAGES_FONT_SIZE = BASE_FONT_SIZE * 0.75
export const PRIMARY_FONT = 'KGNeatlyPrinted'
export const PRIMARY_FONT_SPACED = 'KGNeatlyPrintedSpaced'
export const SECONDARY_FONT = 'Proxima Nova'
