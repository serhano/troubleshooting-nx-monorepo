describe('header: Header component', () => {
    beforeEach(() => cy.visit('/iframe.html?id=header--primary&args=text:Welcome+to+Header'));
      
      it('should render the component', () => {
        cy.get('div').should('contain', 'Welcome to Header');
      });
  });
  